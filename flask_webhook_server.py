from flask import Flask, request, jsonify
import gitlab
import os

app = Flask(__name__)

GITLAB_TOKEN = ${GITLAB_TOKEN} 
GITLAB_PROJECT_ID = ${GITLAB_PROJECT_ID}

@app.route('/webhook-listener', methods=['POST'])
def webhook_listener():
    data = request.json
    if data['object_kind'] == 'issue' and data['event_type'] == 'issue':
        issue_action = data['object_attributes']['action']
        if issue_action == 'open':
            trigger_pipeline()
    return jsonify({'status': 'success'})

def trigger_pipeline():
    gl = gitlab.Gitlab('https://gitlab.com', private_token=GITLAB_TOKEN)
    project = gl.projects.get(GITLAB_PROJECT_ID)
    project.pipelines.create({'ref': 'main'})  # Adjust 'ref' as needed

if __name__ == '__main__':
    app.run(debug=True, port=5000)